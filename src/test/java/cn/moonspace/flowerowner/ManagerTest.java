package cn.moonspace.flowerowner;

import cn.moonspace.common.util.Md5Util;
import cn.moonspace.flowerowner.entities.Manager;
import cn.moonspace.flowerowner.form.LoginForm;
import cn.moonspace.flowerowner.repositories.ManagerRepository;
import cn.moonspace.flowerowner.service.ManagerService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring-core.xml")
public class ManagerTest {

    @Autowired
    ManagerService managerService;
    @Autowired
    ManagerRepository managerRepository;

    @Before
    public void setUp() throws Exception {
        // 数据准备

        // 1、创建admin（admin，admin）
        Manager admin = managerRepository.findByUsername("admin");
        if (admin != null) {
            managerRepository.delete(admin);
        }
        admin = new Manager();
        admin.setUsername("admin");
        admin.setPassword(Md5Util.MD5("52261340"));
        managerRepository.save(admin);

    }

    @Test
    public void testLoginSuccess() {
        // 测试登录成功
        LoginForm form = new LoginForm();
        form.setUsername("admin");
        form.setPassword("52261340");
        int result = managerService.login(form);
        Assert.assertEquals(ManagerService.LOGIN_SUCCESS, result);

    }

}
