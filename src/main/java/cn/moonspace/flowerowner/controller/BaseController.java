package cn.moonspace.flowerowner.controller;

import java.text.SimpleDateFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import cn.moonspace.flowerowner.repositories.CarRepository;
import cn.moonspace.flowerowner.repositories.ManagerRepository;
import cn.moonspace.flowerowner.repositories.PictureRepository;
import cn.moonspace.flowerowner.repositories.RegulationRepository;
import cn.moonspace.flowerowner.repositories.UserRepository;
import cn.moonspace.flowerowner.service.CarService;
import cn.moonspace.flowerowner.service.CommentService;
import cn.moonspace.flowerowner.service.ManagerService;
import cn.moonspace.flowerowner.service.PictureService;
import cn.moonspace.flowerowner.service.RegulationService;
import cn.moonspace.flowerowner.service.UploadService;
import cn.moonspace.flowerowner.service.UserService;

public class BaseController {

	protected Log log = LogFactory.getLog(getClass());

	protected SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	protected String UPLOAD_PICTURE_PATH = "WEB-INF/uploaded/";
	
	protected String PAGE_PATH = "contentPage";
	protected String NEED_LOGIN = "redirect:/login";
	protected String LAYOUT_PAGE = "layout/layout";

	@Autowired
	protected UserRepository userRepository;
	@Autowired
	protected ManagerRepository managerRepository;
	@Autowired
	protected CarRepository carRepository;
	@Autowired
	protected PictureRepository pictureRepository;
	@Autowired
	protected RegulationRepository regulationRepository;
	@Autowired
	protected UserService userService;
	@Autowired
	protected RegulationService regulationService;
	@Autowired
	protected UploadService uploadService;
	@Autowired
	protected CarService carService;
	@Autowired
	protected PictureService pictureService;
	@Autowired
	protected ManagerService managerService;
	@Autowired
	protected CommentService commentService;
	
	
}
