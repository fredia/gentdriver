package cn.moonspace.flowerowner.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.moonspace.flowerowner.form.DriverForm;
import cn.moonspace.flowerowner.form.RegulationForm;
import cn.moonspace.flowerowner.form.UploadForm;


@Controller
public class IndexController extends BaseController {
	
	/**
	 * 主页
	 * 
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value="/index", method=RequestMethod.GET) 
	public String index(ModelMap modelMap) {
		modelMap.addAttribute("regulationForm", new RegulationForm());	// 查违章表单绑定
		modelMap.addAttribute("driverForm", new DriverForm());			// 查扣分表单绑定
		modelMap.addAttribute("uploadForm", new UploadForm());			// 上传表单绑定
		modelMap.addAttribute(PAGE_PATH, "index");
		return LAYOUT_PAGE;
	}

}
