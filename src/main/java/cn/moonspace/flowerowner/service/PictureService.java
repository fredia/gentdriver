package cn.moonspace.flowerowner.service;

import net.sf.json.JSONObject;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import cn.moonspace.flowerowner.exception.DriverException;

public interface PictureService {
	
	@Transactional
	JSONObject uploadPictures(MultipartFile[] files, String cardNo, String cardArea, String description, String realPath) throws DriverException;

}
