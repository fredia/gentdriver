package cn.moonspace.flowerowner.service;

import net.sf.json.JSONObject;

import org.springframework.transaction.annotation.Transactional;

import cn.moonspace.flowerowner.exception.DriverException;
import cn.moonspace.flowerowner.form.DriverForm;
import cn.moonspace.flowerowner.form.RegulationForm;

public interface RegulationService {
	
	@Transactional
	JSONObject searchRegulation(RegulationForm regulationForm) throws DriverException;

	@Transactional
	JSONObject searchDriver(DriverForm driverForm) throws DriverException;

	@Transactional(readOnly=true)
	JSONObject findRegulations(int pages, int pageSize) throws DriverException;
}
