package cn.moonspace.flowerowner.service;

import net.sf.json.JSONObject;

import org.springframework.transaction.annotation.Transactional;

import cn.moonspace.flowerowner.exception.DriverException;

public interface CarService {

	@Transactional(readOnly=true)
	JSONObject findCars(int pages, int pageSize) throws DriverException;
}
