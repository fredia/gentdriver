package cn.moonspace.flowerowner.service.impl;

import org.springframework.stereotype.Service;

import cn.moonspace.common.util.Md5Util;
import cn.moonspace.flowerowner.entities.Manager;
import cn.moonspace.flowerowner.form.LoginForm;
import cn.moonspace.flowerowner.service.ManagerService;

@Service("managerService")
public class ManagerServiceImpl extends BaseService implements ManagerService {
	

	@Override
	public int login(LoginForm loginForm) {
		Manager m = managerRepository.findByUsername(loginForm.getUsername());
		if(null == m) {
			return USERNAME_NOT_FOUND;
		} else if(m.getPassword().equals(Md5Util.MD5(loginForm.getPassword()))){
			return LOGIN_SUCCESS;
		} else {
			return PASSWORD_ERROR;
		}
	}

	@Override
	public int changePass(Manager manager, String oldPass, String newPass) {
		if(!Md5Util.MD5(oldPass).equals(manager.getPassword())) {
			return CHANGE_ERROR;
		} else {
			manager.setPassword(Md5Util.MD5(newPass));
			managerRepository.save(manager);
			return CHANGE_SUCCESS;
		}
	}

}
