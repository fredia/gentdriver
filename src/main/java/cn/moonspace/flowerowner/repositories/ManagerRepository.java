package cn.moonspace.flowerowner.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cn.moonspace.flowerowner.entities.Manager;

public interface ManagerRepository extends JpaRepository<Manager, Long> {

	@Query("from Manager m where m.username = :username")
	Manager findByUsername(@Param("username") String username);
	
}
