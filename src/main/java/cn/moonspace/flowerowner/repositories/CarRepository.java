package cn.moonspace.flowerowner.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cn.moonspace.flowerowner.entities.Car;

public interface CarRepository extends JpaRepository<Car, Long> {
	
	@Query("from Car c where c.cardArea = :cardArea and c.cardNo = :cardNo "
			+ "and c.machineNo = :machineNo and c.cardType = :cardType")
	Car findCar(@Param("cardArea") String cardArea, @Param("cardNo") String cardNo, 
			@Param("machineNo") String machineNo, @Param("cardType") String cardType);

	
}
