package cn.moonspace.flowerowner.entities;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "COMMENT")
public class Comment extends SuperEntity implements Cloneable{

	@Column(name = "IP")
	private String ip;
	@Column(name = "CONTENT")
	private String content;
	@Column(name = "CREATE_DATE")
	private Date createDate;
	
	@ManyToOne
	@JoinColumn(name="UPLOAD_ID")
	private Upload upload;
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Upload getUpload() {
		return upload;
	}
	
	public void setUpload(Upload upload) {
		this.upload = upload;
	}

	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Override
	public Comment clone() throws CloneNotSupportedException {
		Comment comment = new Comment();
		comment.setCreateDate(getCreateDate());
		comment.setId(getId());
		comment.setIp(getIp());
		comment.setContent(getContent());
		return comment;
	}
}
