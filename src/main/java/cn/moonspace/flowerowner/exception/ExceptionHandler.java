package cn.moonspace.flowerowner.exception;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
  
public class ExceptionHandler extends SimpleMappingExceptionResolver {   

    private static ResourceBundle bundle;
	static{
		bundle = PropertyResourceBundle.getBundle("exception");
	}
	
	@Override  
    protected ModelAndView doResolveException(HttpServletRequest request,  
            HttpServletResponse response, Object handler, Exception ex) {  
        // Expose ModelAndView for chosen error view.  
        String viewName = determineViewName(ex, request);  
        if (viewName != null) {// JSP格式返回  
            if (!(request.getHeader("accept").indexOf("application/json") > -1 || (request  
                    .getHeader("X-Requested-With")!= null && request  
                    .getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1))) {  
                // 如果不是异步请求  
                // Apply HTTP status code for error views, if specified.  
                // Only apply it if we're processing a top-level request.  
                Integer statusCode = determineStatusCode(request, viewName);  
                if (statusCode != null) {  
                    applyStatusCodeIfPossible(request, response, statusCode);  
                }  
                return getModelAndView(viewName, ex, request);  
            } else {// JSON格式返回  
                try {  
                	response.setContentType("application/hal+json;charset=UTF-8");
                	String error = "";
                	if (ex instanceof DriverException) {
						DriverException de = (DriverException) ex;
						error = bundle.getString(de.getCode());
					}else{
						error = ex.getMessage();
					}
                    PrintWriter writer = response.getWriter();  
                    writer.write("{\"error\":\"" + error + "\"}");  
                    writer.flush();  
                } catch (IOException e) {  
                    e.printStackTrace();  
                }  
                return null;  
  
            }  
        } else {  
            return null;  
        }  
    }  
}  