<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!-- 查违章----开始 -->
<div id="regulationSearch" class="modal fade" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<form:form id="regulationForm" commandName="regulationForm"
					class="form-horizontal">
					<fieldset>
						<div id="legend" class="component">
							<legend class="component">违章查询</legend>
						</div>
						<div class="form-group">
							<!-- Select Basic -->
							<label class="col-sm-2 control-label">车牌前缀</label>
							<div class="col-sm-4">
								<form:select path="area" class="form-control">
									<option value="京">京</option>
									<option value="沪" selected="">沪</option>
									<option value="港">港</option>
									<option value="吉">吉</option>
									<option value="鲁">鲁</option>
									<option value="冀">冀</option>
									<option value="湘">湘</option>
									<option value="青">青</option>
									<option value="苏">苏</option>
									<option value="浙">浙</option>
									<option value="粤">粤</option>
									<option value="台">台</option>
									<option value="甘">甘</option>
									<option value="川">川</option>
									<option value="黑">黑</option>
									<option value="蒙">蒙</option>
									<option value="新">新</option>
									<option value="津">津</option>
									<option value="渝">渝</option>
									<option value="澳">澳</option>
									<option value="辽">辽</option>
									<option value="豫">豫</option>
									<option value="鄂">鄂</option>
									<option value="晋">晋</option>
									<option value="皖">皖</option>
									<option value="赣">赣</option>
									<option value="闽">闽</option>
									<option value="琼">琼</option>
									<option value="陕">陕</option>
									<option value="云">云</option>
									<option value="贵">贵</option>
									<option value="藏">藏</option>
									<option value="宁">宁</option>
									<option value="桂">桂</option>
								</form:select>
							</div>

							<!-- Select Basic -->
							<label class="col-sm-2 control-label">号牌类型</label>
							<div class="col-sm-4">
								<form:select path="cardType" class="form-control">
									<option value="02/小型汽车号牌" selected="">小型汽车号牌</option>
									<option value="01/大型汽车号牌">大型汽车号牌</option>
									<option value="03/使馆汽车号牌">使馆汽车号牌</option>
									<option value="04/领馆汽车号牌">领馆汽车号牌</option>
									<option value="05/境外汽车号牌">境外汽车号牌</option>
									<option value="06/外籍汽车号牌">外籍汽车号牌</option>
									<option value="07/两、三轮摩托车号牌">两、三轮摩托车号牌</option>
									<option value="08/轻便摩托车号牌">轻便摩托车号牌</option>
									<option value="09/使馆摩托车号牌">使馆摩托车号牌</option>
									<option value="10/领馆摩托车号牌">领馆摩托车号牌</option>
									<option value="11/境外摩托车号牌">境外摩托车号牌</option>
									<option value="12/外籍摩托车号牌">外籍摩托车号牌</option>
									<option value="13/农用运输车号牌">农用运输车号牌</option>
									<option value="14/拖拉机号牌">拖拉机号牌</option>
									<option value="15/挂车号牌">挂车号牌</option>
									<option value="16/教练汽车号牌">教练汽车号牌</option>
									<option value="17/教练摩托车号牌">教练摩托车号牌</option>
									<option value="18/试验汽车号牌">试验汽车号牌</option>
									<option value="19/试验摩托车号牌">试验摩托车号牌</option>
									<option value="20/临时入境汽车号牌">临时入境汽车号牌</option>
									<option value="21/临时入境摩托车号牌">临时入境摩托车号牌</option>
									<option value="22/临时行驶车号牌">临时行驶车号牌</option>
									<option value="23/警用汽车号牌">警用汽车号牌</option>
									<option value="24/警用摩托号牌">警用摩托号牌</option>
								</form:select>
							</div>

						</div>

						<div class="form-group">

							<!-- Text input-->
							<label class="col-sm-2 control-label" for="input01">车牌号</label>
							<div class="col-sm-4">
								<form:input path="cardNo" type="text" placeholder="请输入您的车牌号" class="form-control" />
								<p class="help-block"></p>
							</div>

							<!-- Text input-->
							<label class="col-sm-2 control-label" for="input01">发动机号</label>
							<div class="col-sm-4">
								<form:input path="machineNo" type="text" placeholder="请输入您的发动机号" class="form-control" />
								<p class="help-block"></p>
							</div>

						</div>

						<div class="form-group">
							<label class="col-sm-5 control-label"></label>

							<!-- Button -->
							<div class="col-sm-5">
								<label id="search-btn" class="btn btn-info"
									data-loading-text="查询中...">查询</label>
							</div>
						</div>

					</fieldset>
				</form:form>

				<div id="regulations"></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#search-btn').click(function () {
		
		var btn = $(this);
	    btn.button('loading');
	    
	    $('#reglInfo').remove();
	    
		$.post("search/regulations", $("#regulationForm").serialize(), function (data) {
			if(data.error != null) {
				$("#regulations").append('<div id="reglInfo" class="alert alert-warning alert-dismissible fade in">' + data.error + '</div>');
			} else {
				var regulations = data.result;
				for(var i = 0; i < regulations.length; i ++) {
					var regulation = regulations[i];
					var table = 
						'<table class="table table-condensed table-striped">'+
							'<caption>' + regulation.title + '</caption>' + 
							'<tr>' + 
								'<th width="80px">' + '违法次数' + '</th>' + 
								'<th width="80px">' + '车牌号' + '</th>' + 
								'<th>' + regulation.cardNumber + '</th>' + 
								'<th>' + '号牌类型' + '</th>' + 
								'<th>' + regulation.cardType + '</th>' + 
							'</tr>';
					if(regulation.count == '0') {
						table += '<tr><td colspan="5" align="center">' + regulation.message + '</td></tr>';
					} else {
						for(var j = 0; j < regulation.count; j ++) {
							var regulationInfo = regulation.list[j];
							table += 
								'<tr>' + 
									'<td rowspan="5">' + (j + 1) + '</td>' + 
									'<td>' + '违法时间' + '</td>' + 
									'<td>' + regulationInfo.time + '</td>' + 
									'<td>' + '凭证编号' + '</td>' + 
									'<td>' + regulationInfo.seq + '</td>' + 
								'</tr>' + 
								'<tr>' + 
									'<td>' + '违法地点' + '</td>' + 
									'<td>' + regulationInfo.address + '</td>' + 
									'<td>' + '采集机关' + '</td>' + 
									'<td>' + regulationInfo.dept + '</td>' + 
								'</tr>' + 
								'<tr>' + 
									'<td>' + '违法内容' + '</td>' + 
									'<td colspan="3">' + regulationInfo.content + '</td>' + 
								'</tr>' + 
								'<tr>' + 
									'<td>' + '违反条款' + '</td>' + 
									'<td colspan="3">' + regulationInfo.rule + '</td>' + 
								'</tr>' + 
								'<tr>' + 
									'<td>' + '状态' + '</td>' + 
									'<td colspan="3">' + regulationInfo.status + '</td>' + 
								'</tr>';
						}
					}
					table += '</table>';
						
					$("#regulations").append('<div id="reglInfo" class="alert alert-warning alert-dismissible fade in">' + table + '</div>');
				}
			}
			
			btn.button('reset');
			
		}, "json");
	});
});
</script>
<!-- 查违章----结束 -->