<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>GentDriver Management</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <link rel="shortcut icon" href="ico/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/font-awesome.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/back.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger-theme-ice.css" />
	<script src="<%=request.getContextPath()%>/jquery/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/messenger/js/messenger.min.js"></script>
	
</head>

<body>
	<jsp:include page="header.jsp"></jsp:include>
	
	<aside id="sidebar" class="column">
	  	<jsp:include page="navbar.jsp"></jsp:include>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
	</section>
</body>

</html>