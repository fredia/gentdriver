<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="myNavbar">

		<h3>奇葩审核</h3>
		<ul class="toggle">
			<li class="icn_edit_article"><a href="verify?status=0">未审核列表</a></li>
			<li class="icn_edit_article"><a href="verify?status=1">审核通过列表</a></li>
			<li class="icn_edit_article"><a href="verify?status=2">审核未通过列表</a></li>
		</ul>
		
		<h3>违章查看</h3>
		<ul class="toggle">
			<li class="icn_categories"><a href="car">车辆列表</a></li>
			<li class="icn_categories"><a href="regulation">违章信息列表</a></li>
		</ul>
		
</div>