<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>GentDriver Management</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <link rel="shortcut icon" href="ico/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/font-awesome.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/back.css" />
	<script src="<%=request.getContextPath()%>/jquery/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
</head>

<body>

	<div class="container">
		<form:form id="loginForm" commandName="loginForm" class="form-signin">
			<h2 class="form-signin-heading">Please sign in</h2>
			<div class="input-group margin-bottom-sm">
				<span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i> 帐号</span>
				<form:input path="username" type="text" placeholder="请输入用户名" class="form-control" />
			</div>
			<p id="usernameHelp" style="color: red;" class="help-block"><form:errors path="username" cssStyle="color:red"/> </p>
			<div class="input-group margin-bottom-sm">
				<span class="input-group-addon"><i class="fa fa-key fa-fw"></i> 密码</span>
				<form:input path="password" type="password" placeholder="请输入密码" class="form-control" />
			</div>
			<p id="passwordHelp" style="color: red;" class="help-block"></p>
			<label id="login-btn" class="btn btn-lg btn-primary btn-block" style="margin-top:10px;">登录</label>
		</form:form>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		
		$('#login-btn').click(function () {
			$('#loginForm p').html("");
			$.post("login", $("#loginForm").serialize(), function (data) {
				if(data.error != null) {
					// 有错误，更新错误信息
					for(var i = 0; i < data.error.length; i ++) {
						$('#loginForm #' + data.error[i].field).append(data.error[i].message);
					}
				} else {
					// 无错误，提示登录成功，并重新跳转到首页
					window.location.href="index";
				}
				
			}, "json");
		});
		
		$(window).keydown(function(event){
		    switch(event.keyCode) {
		    	case 13:
				$('#login-btn').click();
		    }
		});
	});
</script>
</html>
