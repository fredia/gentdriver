<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<link rel="shortcut icon" href="ico/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/buttons.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/gallery.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/home.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/font-awesome.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/load-more.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/fileinput/css/fileinput.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger-theme-future.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger-theme-ice.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/messenger/css/messenger-theme-air.css" />
<style type="text/css">
.jumbotron h1{
	font-size: 40px;
}
</style>
<script src="<%=request.getContextPath()%>/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/fileinput/js/fileinput.min.js"></script>
<script src="<%=request.getContextPath()%>/messenger/js/messenger.min.js"></script>
<script src="<%=request.getContextPath()%>/messenger/js/messenger-theme-future.js"></script>
<script src="<%=request.getContextPath()%>/javascript/buttons.js"></script>
<script src="<%=request.getContextPath()%>/javascript/dropdown.js"></script>
<script src="<%=request.getContextPath()%>/javascript/gallery-scroll.js"></script>
<script src="<%=request.getContextPath()%>/javascript/unslider.js"></script>
<title>文明车主</title>
</head>
<body>
	  <jsp:include page="../${contentPage}.jsp"></jsp:include>
      
	<div class="container">
      <div class="footer" style="text-align: center">
        <p> Copyright © 2014 gentdriver.com  沪ICP备14037150号</p>
      </div>

    </div>
	<!-- /container -->
<script type="text/javascript">
var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F2772c77a423cdbd336e61b0c9641280f' type='text/javascript'%3E%3C/script%3E"));
</script>
</body>
</html>

