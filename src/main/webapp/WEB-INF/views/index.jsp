<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!--主页面-->
<section class="content1">
<!--导航条-->
  <nav class="navbar" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand brand" href="#"><img src="images/logo3.png" class="logo"/>文明车主</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#" id="shangchuanqipa"><span class="icon-upload-alt"></span> 上传奇葩</a></li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="icon-search"></span>违章查询<span class="caret"></span></a>
          <div class="dropdown-menu">
          <div class="block" style="padding:10px;">
          <form:form id="regulationForm" commandName="regulationForm">
			<fieldset>
            	<div class="form-group">
                <form:select path="area" class="form-control">
							<option value="京">京</option>
							<option value="沪" selected="">沪</option>
							<option value="港">港</option>
							<option value="吉">吉</option>
							<option value="鲁">鲁</option>
							<option value="冀">冀</option>
							<option value="湘">湘</option>
							<option value="青">青</option>
							<option value="苏">苏</option>
							<option value="浙">浙</option>
							<option value="粤">粤</option>
							<option value="台">台</option>
							<option value="甘">甘</option>
							<option value="川">川</option>
							<option value="黑">黑</option>
							<option value="蒙">蒙</option>
							<option value="新">新</option>
							<option value="津">津</option>
							<option value="渝">渝</option>
							<option value="澳">澳</option>
							<option value="辽">辽</option>
							<option value="豫">豫</option>
							<option value="鄂">鄂</option>
							<option value="晋">晋</option>
							<option value="皖">皖</option>
							<option value="赣">赣</option>
							<option value="闽">闽</option>
							<option value="琼">琼</option>
							<option value="陕">陕</option>
							<option value="云">云</option>
							<option value="贵">贵</option>
							<option value="藏">藏</option>
							<option value="宁">宁</option>
							<option value="桂">桂</option>
						</form:select>
                </div>
				<div class="form-group">
					<form:input path="cardNo" type="text" placeholder="请输入车牌号" class="form-control" />
				</div>
                <div class="form-group">
                <form:select path="cardType" class="form-control">
							<option value="02/小型汽车号牌" selected="">小型汽车号牌</option>
							<option value="01/大型汽车号牌">大型汽车号牌</option>
							<option value="03/使馆汽车号牌">使馆汽车号牌</option>
							<option value="04/领馆汽车号牌">领馆汽车号牌</option>
							<option value="05/境外汽车号牌">境外汽车号牌</option>
							<option value="06/外籍汽车号牌">外籍汽车号牌</option>
							<option value="07/两、三轮摩托车号牌">两、三轮摩托车号牌</option>
							<option value="08/轻便摩托车号牌">轻便摩托车号牌</option>
							<option value="09/使馆摩托车号牌">使馆摩托车号牌</option>
							<option value="10/领馆摩托车号牌">领馆摩托车号牌</option>
							<option value="11/境外摩托车号牌">境外摩托车号牌</option>
							<option value="12/外籍摩托车号牌">外籍摩托车号牌</option>
							<option value="13/农用运输车号牌">农用运输车号牌</option>
							<option value="14/拖拉机号牌">拖拉机号牌</option>
							<option value="15/挂车号牌">挂车号牌</option>
							<option value="16/教练汽车号牌">教练汽车号牌</option>
							<option value="17/教练摩托车号牌">教练摩托车号牌</option>
							<option value="18/试验汽车号牌">试验汽车号牌</option>
							<option value="19/试验摩托车号牌">试验摩托车号牌</option>
							<option value="20/临时入境汽车号牌">临时入境汽车号牌</option>
							<option value="21/临时入境摩托车号牌">临时入境摩托车号牌</option>
							<option value="22/临时行驶车号牌">临时行驶车号牌</option>
							<option value="23/警用汽车号牌">警用汽车号牌</option>
							<option value="24/警用摩托号牌">警用摩托号牌</option>
						</form:select>
                        </div>
                    <div class="form-group">
						<form:input path="machineNo" type="text" placeholder="请输入发动机号" class="form-control" />
					</div>
                    <hr>
					<label id="search-btn" class="btn btn-success btn-block"
								data-loading-text="查询中...">查询</label>
			</fieldset>
		</form:form>
          </div>
          </div>
        </li>
        
        <li><a href="#koufenchaxun"><span class="icon-search"></span> 扣分查询</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="icon-mobile-phone" style="font-size:20px;"></span> App下载 <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#IOSdownload"><img src="images/apple.png"/> Ios下载</a></li>
            <li class="divider"></li>
            <li><a href="#"><img src="images/android.png"/> Android下载</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="container">
<h1>“车位已如此紧张，且停且珍惜”</h1>
<h3>输入您或朋友的牌照，看看有没有不良的记录？</h3>
<div class="row">
<div class="col-md-7">
			<!--奇葩查询提交-->
            <div class="search-block">
                <div class="row">
                <form action="gallery" method="post" enctype="application/x-www-form-urlencoded">
	                <div class="col-md-9">
	    			<input class="form-control1 form-control" placeholder="请输入牌照（如沪F12345）" type="text" name="search" required="required">
	    			<input type="hidden" name="pages" value="0">
	    			<input type="hidden" name="pageSize" value="8">
	                </div>
	                <div class="col-md-3">
	    			<button id="search-gallery" type="submit" class="button button-rounded button-primary btn-search" style="height:46px;font-size:18px"><span class="icon-search"></span> 搜 索</button>
	                </div>
                </form>
                </div>
        	</div>
</div>
</div>
</div>
</section>

<div class="gallery-thumbnail">
	<div id="gallery-result"></div>
</div>

<section id="koufenchaxun" class="content4">
<div class="row">
<h1 class="col-md-4" style="padding-left:20%;color:#B8B8B8;">扣分查询</h1>
<p class="col-md-8" style="padding:20px;color:#B7B7B7;"><span class="icon-exclamation-sign"></span>
 注意：凡使用旧版驾驶证的司机朋友查询时需要在原八位档案编号前加3100，<br>即如果您的档案编号为04119602,那么您需要输入310004119602方能查询。</p>
</div>
<form:form id="driverForm" commandName="driverForm">
<fieldset>
<div class="search-block2">

    			<input class="form-control1 form-control" placeholder="请输入驾驶员档案编号" type="text" name="driverNo">
</div>
				<label id="search-btn2" class="btn btn-search btn-success btn-block button1" style="height:44px;font-size:18px" data-loading-text="查询中..."><span class="icon-search"></span> 搜 索</label>
    			</fieldset>
				</form:form>
				</section>
<div id="regulationSearch" class="modal fade" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div id="regulations"></div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog" style="width:940px;">
	    <div class="modal-content" id="gallery-detail">
	      
	    </div>
	  </div>
	</div>
	
	<div id="upload" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
			        <form:form action="upload/picture" id="uploadForm" enctype="multipart/form-data" commandName="uploadForm" class="form-horizontal">
				        <div class="form-group">
							<label for="plateNo" class="col-sm-2 control-label">车牌号</label>
							<div id="plateNo" class="col-sm-2">
								<form:select path="cardArea" class="form-control">
									<option value="京">京</option>
									<option value="沪" selected="selected">沪</option>
									<option value="港">港</option>
									<option value="吉">吉</option>
									<option value="鲁">鲁</option>
									<option value="冀">冀</option>
									<option value="湘">湘</option>
									<option value="青">青</option>
									<option value="苏">苏</option>
									<option value="浙">浙</option>
									<option value="粤">粤</option>
									<option value="台">台</option>
									<option value="甘">甘</option>
									<option value="川">川</option>
									<option value="黑">黑</option>
									<option value="蒙">蒙</option>
									<option value="新">新</option>
									<option value="津">津</option>
									<option value="渝">渝</option>
									<option value="澳">澳</option>
									<option value="辽">辽</option>
									<option value="豫">豫</option>
									<option value="鄂">鄂</option>
									<option value="晋">晋</option>
									<option value="皖">皖</option>
									<option value="赣">赣</option>
									<option value="闽">闽</option>
									<option value="琼">琼</option>
									<option value="陕">陕</option>
									<option value="云">云</option>
									<option value="贵">贵</option>
									<option value="藏">藏</option>
									<option value="宁">宁</option>
									<option value="桂">桂</option>
								</form:select>
							</div>
							<div class="col-sm-7">
								<form:input path="cardNo" type="text" placeholder="请输入车牌号" class="form-control" />
								<p class="help-block"></p>
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="col-sm-2 control-label">吐槽区</label>
							<div class="col-sm-9">
								<form:textarea path="description" placeholder="尽情吐槽吧" class="form-control" rows="5"></form:textarea>
								<p class="help-block"></p>
							</div>
				        </div>
						<div class="form-group">
							<label for="files" class="col-sm-2 control-label">图片区</label>
						    <div class="col-sm-9">
				            	<form:input path="files" type="file" multiple="true" data-preview-file-type="any" />
						    </div>
				        </div>
				        <div class="form-group">
				        <div class="col-sm-offset-2 col-sm-9">
				            <label id="upload-btn" class="btn btn-primary" data-loading-text="提交中...">提交</label>
				            <button class="btn btn-default" type="reset">重置</button>
				            </div>
				        </div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
	
    <script type="text/javascript"> 
	    $('.dropdown-toggle').dropdown(); 
		$("#input-id").fileinput();
		$('#files').fileinput({
	       overwriteInitial: false,
	       maxFileSize: 2000,
	       maxFilesNum: 1
		});
    </script> 
	<script type="text/javascript">
		$.fn.transGallery=function(gallery){
			if(gallery == undefined || gallery == null){
				return "";
			}
			var table = '';
			for(var i = 0; i < gallery.length; i ++) {
				var result = gallery[i];
				
				var detail = '';
				detail += result.id + '/' + result.cardArea + result.cardNo + '/' + result.pictures.length + '/' + result.createDate + '/' + result.description + "/";
				for(j = 0; j < result.pictures.length; j++){
					detail += result.pictures[j].thumbPathLarge;
					if(j < result.pictures.length - 1)
						detail += "-";
				}
				
				if(i%4 == 0){
					table += '<div id="gallery-row" class="row">';
				}
				table += 
					'<div class="col-xs-6 col-md-3">' + 
						'<div class="thumbnail">' + 
							//'<img data-src="holder.js/100%180" src="uploaded/' + result.pictures[0].thumbPath+ '" >' +
		                    '<img class="modalTag" index="' + i +'" data-src="holder.js/100%180" style="cursor:pointer;background-image:url(uploaded/'
		                          + result.pictures[0].thumbPath + ');background-repeat:no-repeat;background-position:center;height:200px;width:100%;" onclick=detail("' + detail + '") />' +
							'<p class="text1">' + result.cardArea + result.cardNo + '<span class="text4">(' + result.pictures.length + '张图)</span><span class="text3">' + result.createDate + '</span></p>' + 
							'<p class="text2" style="word-wrap:break-word; word-break:normal;overflow:auto;">' + cutStr(result.description,30) + '</p>' +
						'</div>' +
					'</div>';
				if(i%4 == 3 || i == gallery.length - 1){
					table += '</div>';
				}
			}
			if(gallery.length == 4){
				table += '<div class="row">' +
							'<span style="display:block; text-align:center;margin-bottom: 15px;">' +
								'<button style="width: 108px;" type="button" id="load_more" class="getmore btn btn-default btn-outline btn-back-white smooth color" onclick="window.location.href=\'gallery\'">更 多</button>' +
							'</span>' +
						'</div>';
			}
			return table;
		}
		$(document).ready(function(){
			var messenger = Messenger({extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',theme:'ice'}).post().hide();
			$.ajax({
		      url: 'galleryAjax',
		      type: 'POST',
		      data: {search:'', pages:0, pageSize:4},
		      success:function(data){
		      	if(data.results == null && data.error != null){ 
		      		messenger.update({
						message: data.error,
						type: '',
						hideAfter: 5,
 						hideOnNavigate: true,
						showCloseButton: true
					}).show();
		      	}else if(data.results == null && data.error == null && data != null){
		      		var dataObj=eval("("+data+")");
		      		messenger.update({
						message: dataObj.error,
						type: '',
						hideAfter: 5,
 						hideOnNavigate: true,
						showCloseButton: true
					}).show();
		      	} else {
					var gallery = data.results;
					var table = $(this).transGallery(gallery);
					$("#gallery-result").append(table);
				}
		      }
			}); 
			
			// 查违章事件
			$('#search-btn').click(function () {
				
				var btn = $(this);
			    btn.button('loading');
			    
			    $('#reglInfo').remove();
			    
				 $.ajax( {
				      url: 'search/regulations',
				      type: 'POST',
				      data: $("#regulationForm").serialize(),
				      success:function(data){
				//$.post("search/regulations", $("#regulationForm").serialize(), function (data) {
					if(data.error != null) {
						$("#regulations").append('<div id="reglInfo" class="alert alert-warning alert-dismissible fade in">' + data.error + '</div>');
						$('#regulationSearch').modal();
					} else if(data.result == null && data != null){
						var dataObj=eval("("+data+")");
						messenger.update({
							message: dataObj.error,
							type: '',
							hideAfter: 5,
	 						hideOnNavigate: true,
							showCloseButton: true
						}).show();
					}else {
						var regulations = data.result;
						for(var i = 0; i < regulations.length; i ++) {
							var regulation = regulations[i];
							var table = 
								'<table class="table table-condensed table-striped">'+
									'<caption>' + regulation.title + '</caption>' + 
									'<tr>' + 
										'<th width="80px">' + '违法次数' + '</th>' + 
										'<th width="80px">' + '车牌号' + '</th>' + 
										'<th width="80px">' + regulation.cardNumber + '</th>' + 
										'<th width="80px">' + '号牌类型' + '</th>' + 
										'<th width="80px">' + regulation.cardType + '</th>' + 
									'</tr>';
							if(regulation.count == '0') {
								table += '<tr><td colspan="5" align="center">' + regulation.message + '</td></tr>';
							} else {
								for(var j = 0; j < regulation.count; j ++) {
									var regulationInfo = regulation.list[j];
									table += 
										'<tr>' + 
											'<td rowspan="5">' + (j + 1) + '</td>' + 
											'<td>' + '违法时间' + '</td>' + 
											'<td>' + regulationInfo.time + '</td>' + 
											'<td>' + '凭证编号' + '</td>' + 
											'<td>' + regulationInfo.seq + '</td>' + 
										'</tr>' + 
										'<tr>' + 
											'<td>' + '违法地点' + '</td>' + 
											'<td>' + regulationInfo.address + '</td>' + 
											'<td>' + '采集机关' + '</td>' + 
											'<td>' + regulationInfo.dept + '</td>' + 
										'</tr>' + 
										'<tr>' + 
											'<td>' + '违法内容' + '</td>' + 
											'<td colspan="3">' + regulationInfo.content + '</td>' + 
										'</tr>' + 
										'<tr>' + 
											'<td>' + '违反条款' + '</td>' + 
											'<td colspan="3">' + regulationInfo.rule + '</td>' + 
										'</tr>' + 
										'<tr>' + 
											'<td>' + '状态' + '</td>' + 
											'<td colspan="3">' + regulationInfo.status + '</td>' + 
										'</tr>';
								}
							}
							table += '</table>';
								
							$("#regulations").append('<div id="reglInfo" class="alert alert-warning alert-dismissible fade in">' + table + '</div>');
						
						}
						$('#regulationSearch').modal();
					}
					
					btn.button('reset');
					
					}
				});
			}); // 查违章事件处理结束
			
			// 查扣分事件	
			$('#search-btn2').click(function () {
				
				var btn = $(this);
			    btn.button('loading');
			    
			    $('#reglInfo').remove();
			    
				//$.post("search/driver", $("#driverForm").serialize(), function (data) {
			    $.ajax( {
				      url: 'search/driver',
				      type: 'POST',
				      data: $("#driverForm").serialize(),
				      success:function(data){
					
						if(data.error != null) {
							$("#regulations").append('<div id="reglInfo" class="alert alert-warning alert-dismissible fade in">' + data.error + '</div>');
							$('#regulationSearch').modal();
						} else if(data.result == null && data != null){
							var dataObj=eval("("+data+")");
							messenger.update({
								message: dataObj.error,
								type: '',
								hideAfter: 5,
		 						hideOnNavigate: true,
								showCloseButton: true
							}).show();
						}else { 
							//  修改成组织具体扣分内容
							var title = data.driverNo + "&nbsp;&nbsp;&nbsp;" + data.driverName + "&nbsp;&nbsp;&nbsp;" + data.sumScore;
							var table = 
								'<table class="table table-condensed table-striped">'+
									'<caption>' + title + '</caption>' + 
									'<tr>' + 
										'<th width="80px">' + '序号' + '</th>' + 
										'<th width="80px" colspan="4">' + '违法信息' + '</th>' + 
									'</tr>';
						
							for(var i = 0; i < data.items.length; i ++) {
								var regulationInfo = data.items[i];
								table += 
									'<tr>' + 
										'<td rowspan="9">' + (i + 1) + '</td>' + 
										'<td>' + '处理机关(机关代码)' + '</td>' + 
										'<td>' + regulationInfo.processDept + '</td>' + 
										'<td>' + '文书编号' + '</td>' + 
										'<td>' + regulationInfo.seq + '</td>' + 
									'</tr>' + 
									'<tr>' + 
										'<td>' + '号牌号码' + '</td>' + 
										'<td>' + regulationInfo.cardNo + '</td>' + 
										'<td>' + '号牌种类' + '</td>' + 
										'<td>' + regulationInfo.cardType + '</td>' + 
									'</tr>' + 
									'<tr>' + 
										'<td>' + '姓    名' + '</td>' + 
										'<td>' + regulationInfo.name + '</td>' + 
										'<td>' + '违法时间' + '</td>' + 
										'<td>' + regulationInfo.time + '</td>' + 
									'</tr>' + 
									'<tr>' + 
										'<td>' + '违法地址' + '</td>' + 
										'<td colspan="3">' + regulationInfo.address + '</td>' + 
									'</tr>' +
									'<tr>' + 
										'<td>' + '违法内容' + '</td>' + 
										'<td colspan="3">' + regulationInfo.content + '</td>' + 
									'</tr>' +
									'<tr>' + 
										'<td>' + '违反条款' + '</td>' + 
										'<td colspan="3">' + regulationInfo.rule + '</td>' + 
									'</tr>' + 
									'<tr>' + 
										'<td>' + '处罚依据' + '</td>' + 
										'<td colspan="3">' + regulationInfo.punishment + '</td>' + 
									'</tr>' + 
									'<tr>' + 
										'<td>' + '处罚内容' + '</td>' + 
										'<td colspan="3">' + regulationInfo.penalty + '</td>' + 
									'</tr>'  + 
									'<tr>' + 
										'<td>' + '记分分值' + '</td>' + 
										'<td colspan="3">' + regulationInfo.score + '</td>' + 
									'</tr>';
								
							}
							table += '</table>';
									
							$("#regulations").append('<div id="reglInfo" class="alert alert-warning alert-dismissible fade in">' + table + '</div>');
							$('#regulationSearch').modal();
						}
						btn.button('reset');
					}
				});
			}); // 查扣分事件处理结束
			
			$('#shangchuanqipa').click(function () {
				$('#upload').modal();
					
				// 上传奇葩
				$('#upload-btn').click(function () {
					
					if($('#files').val() == ""){
						messenger.update({
							message: '请上传文件',
							type: '',
							hideAfter: 5,
  							hideOnNavigate: true,
							showCloseButton: true
						}).show();
						return false;
					}
					
					var btn = $(this);
				    btn.button('loading');
					
				    $.ajax( {
				      url: 'upload/picture',
				      type: 'POST',
				      data: new FormData($('#uploadForm')[0]),
				      processData: false,
				      contentType: false,
				      success:function(msg){
				      	if(msg.message == null && msg.error != null){ 
				      		messenger.update({
								message: msg.error,
								type: '',
								hideAfter: 5,
	  							hideOnNavigate: true,
								showCloseButton: true
							}).show();
				      	}else if(msg.message == null && msg.error == null && msg != null){
				      		var dataObj=eval("("+msg+")");
				      		messenger.update({
								message: dataObj.error,
								type: '',
								hideAfter: 5,
	  							hideOnNavigate: true,
								showCloseButton: true
							}).show();
				      	}else{
				      		messenger.update({
								message: msg.message,
								type: '',
								hideAfter: 5,
	  							hideOnNavigate: true,
								showCloseButton: true
							}).show();
				      	}
						$('#upload').modal('hide');
						$('#uploadForm').get(0).reset();
						btn.button('reset');
				      },
				      error:function(msg){
				      	messenger.update({
							message: msg.statusText,
							type: '',
							hideAfter: 5,
  							hideOnNavigate: true,
							showCloseButton: true
						}).show();
						btn.button('reset');
				      }
				    } );
					
				}); // 上传奇葩表单展示
			}); 
		});
	</script>
<script type="text/javascript"> 
	function detail(result){
	    $("#gallery-detail").html('');
	    
	    var results = result.split("/");
	    var pics = results[5].split("-");
	    
	    var detail = 	'<div class="modal-header">' + 
		        			'<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + 
		        			'<h4 class="modal-title" id="myModalLabel">' + results[1] + ' (' + results[2] + '张图)<span class="text5">' + results[3] + '</span></h4>' +
		      			'</div>' +
		      			'<div class="modal-body">' +
		      				'<div style="width: 330px; height: 400px; display: inline; float: right; border-left: 1px solid #e5e5e5; padding-left: 10px; font-size: x-small;">' +
								'<div id="comment" style="width: 330px; height: 360px; overflow: auto;">' +
								'</div>' +
								'<div class="input-group" style="padding-top: 5px;">' +
									'<input id="commentContent" type="text" class="form-control" />' +
									'<div class="input-group-btn">' +
										'<button id="sendComment" class="btn btn-primary">发送</button>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<div class="banner has-dots" style="overflow: hidden; width: 570px;">' +
	                			'<ul style="position: relative; left: -100%; height: 400px; padding-left:0px;">';
        for(var i = 0; i < pics.length; i++){
			detail += '<li style="height:400px;background-image:url(uploaded/' + pics[i] + ');background-repeat:no-repeat;background-position:center;background-size:contain;"></li>'
        }
        detail +=			'</ul>' +
						'</div>' +
	      			'</div>' +
	      			'<div class="modal-footer">' +
	      				'<p style="text-align:left;overflow:auto;">' + results[4] + '</p>' + 
	     			'</div>';
	    $("#gallery-detail").append(detail);
	    $("#myModal").modal();
	    
	    $('#sendComment').click(function () {
	    	if($('#commentContent').val().length == 0){
	    		return false;
	    	}
	    	var messenger = Messenger({extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',theme:'ice'}).post().hide();
	    	$.ajax( {
				      url: 'comment/add',
				      type: 'POST',
				      data: {uploadID:results[0],content:$('#commentContent').val()},
				      success:function(msg){
				      	if(msg.result == null && msg.error != null){ 
				      		messenger.update({
								message: msg.error,
								type: '',
								hideAfter: 5,
	  							hideOnNavigate: true,
								showCloseButton: true
							}).show();
				      	}else if(msg.result == null && msg.error == null && msg != null){
				      		var dataObj=eval("("+msg+")");
				      		messenger.update({
								message: dataObj.error,
								type: '',
								hideAfter: 5,
	  							hideOnNavigate: true,
								showCloseButton: true
							}).show();
				      	}else{
				      		messenger.update({
								message: '评论成功',
								type: '',
								hideAfter: 5,
	  							hideOnNavigate: true,
								showCloseButton: true
							}).show();
							$('#commentContent').val('');
							var commentAppend = '';
							commentAppend += '<div	style="padding-bottom: 10px; margin-bottom: 12px; border-bottom: 1px solid #e5e5e5; font-family: "Microsoft YaHei", "微软雅黑", helvetica, arial, verdana, tahoma, sans-serif;">' +
												'<p style="color: #06a7e1;">' + msg.result.ip + '</p>' +
												'<div class="con">' +
													'<div style="margin-bottom: 10px; width: 300px;">' +
														'<p>' + msg.result.content + '</p>' +
													'</div>' +
													'<span style="color: #909090;">' + msg.result.createDate + '</span>' +
												'</div>' + 
											'</div>';
							if($('#comment > div:first-child').size() == 0){
								$('#comment').append(commentAppend);
							}else{
								$('#comment > div:first-child').before(commentAppend);
							}
				      	}
				      }
				     });
	    });
	     
		$('.banner').unslider({
			speed: 500,               //  The speed to animate each slide (in milliseconds)
			delay: 3000,              //  The delay between slide animations (in milliseconds)
			complete: function() {},  //  A function that gets called after every slide animation
			keys: true,               //  Enable keyboard (left, right) arrow shortcuts
			dots: true,               //  Display dot navigation
			fluid: false              //  Support responsive design. May break non-responsive designs
		});
	    
	    $.ajax( {
		      url: 'comment/get',
		      type: 'GET',
		      data: {uploadID:results[0]},
		      success:function(data){
		      	if(data.error != null) {
				} else {
					var comments = data.results;
					var comTab = '';
					for(var i = 0; i < comments.length; i ++) {
						var com = comments[i];
						comTab += '<div	style="padding-bottom: 10px; margin-bottom: 12px; border-bottom: 1px solid #e5e5e5; font-family: "Microsoft YaHei", "微软雅黑", helvetica, arial, verdana, tahoma, sans-serif;">' +
										'<p style="color: #06a7e1;">' + com.ip + '</p>' +
										'<div class="con">' +
											'<div style="margin-bottom: 10px; width: 300px;">' +
												'<p>' + com.content + '</p>' +
											'</div>' +
											'<span style="color: #909090;">' + com.createDate + '</span>' +
										'</div>' + 
									'</div>';
					}
					$('#comment').append(comTab);
				}
		      }
			});
	    
	}
	function cutStr(str, len){
	    if(str.length > len){
	          return str.substring(0,len)+"......";
	     }
	     return str;
	}
	$(window).keydown(function(event){
		    switch(event.keyCode) {
		    	case 13:
		    	if($('#commentContent').is(":focus")){
					$('#sendComment').click();
				}
		    }
		});  
    </script> 
