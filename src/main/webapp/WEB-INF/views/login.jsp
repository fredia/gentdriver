<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div id="loginModal" class="modal fade" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<form:form id="loginForm" commandName="loginForm" class="form-horizontal">
					<fieldset>
						<div id="legend" class="component">
							<legend class="component">登录</legend>
						</div>
						<div class="form-group">

							<!-- Text input-->
							<label class="col-sm-4 control-label">用户名</label>
							<div class="col-sm-7">
								<form:input path="username" type="text" placeholder="请输入用户名" class="form-control" />
								<p id="usernameHelp" style="color: red;" class="help-block"><form:errors path="username" cssStyle="color:red"/> </p>
							</div>
						</div>

						<div class="form-group">

							<!-- Text input-->
							<label class="col-sm-4 control-label">密码</label>
							<div class="col-sm-7">
								<form:input path="password" type="password" placeholder="请输入密码" class="form-control" />
								<p id="passwordHelp" style="color: red;" class="help-block"></p>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label"></label>

							<!-- Button -->
							<div class="col-sm-7">
								<label id="login-btn" class="btn btn-info">登录</label>
							</div>
						</div>

					</fieldset>
				</form:form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#login-btn').click(function () {
		$('#loginModal p').html("");
		
		$.post("user/login", $("#loginForm").serialize(), function (data) {
			if(data.error != null) {
				// 有错误，更新错误信息
				for(var i = 0; i < data.error.length; i ++) {
					$('#loginForm #' + data.error[i].field).html("");
					$('#loginForm #' + data.error[i].field).append(data.error[i].message);
				}
			} else {
				// 无错误，提示登录成功，并重新跳转到首页
				alert("登录成功");
				window.location.href="index";
			}
			
		}, "json");
	});
});
</script>